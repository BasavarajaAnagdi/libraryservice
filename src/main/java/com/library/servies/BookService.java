package com.library.servies;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.domains.BookDomain;
import com.library.entities.BookEntity;
import com.library.entities.BorrowEntity;
import com.library.entities.UserEntity;
import com.library.exception.ExceptionDomain;
import com.library.exception.LibraryServiceException;
import com.library.repositories.BookRepository;
import com.library.repositories.BorrowRepository;

import io.jsonwebtoken.lang.Collections;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	BorrowRepository borrowRepository;

	@Autowired
	UserService userService;

	public void addBook(BookDomain bookDomain, long userId) {
		if (userService.adminUser(userId)) {
			BookEntity bookEntity = bookRepository.findByISBNCode(bookDomain.getIsbnCode());
			if (bookEntity == null) {
				bookEntity = new BookEntity();
				bookEntity.setBookName(bookDomain.getBookName());
				bookEntity.setAuthorName(bookDomain.getAuthorName());
				bookEntity.setCategory(bookDomain.getCategory());
				bookEntity.setIsbnCode(bookDomain.getIsbnCode());
				bookEntity.setPublisher(bookDomain.getPublisher());
				bookRepository.save(bookEntity);
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("Book already exists", 1005);
				throw new LibraryServiceException(exceptionDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid admin user", 1006);
			throw new LibraryServiceException(exceptionDomain);
		}

	}

	public void updateBook(BookDomain bookDomain, long userId) {
		if (userService.adminUser(userId)) {
			if (bookDomain.getIsbnCode() != null) {
				BookEntity bookEntity = bookRepository.findByISBNCode(bookDomain.getIsbnCode());
				if (bookEntity != null) {
					bookEntity.setBookName(bookDomain.getBookName());
					bookEntity.setAuthorName(bookDomain.getAuthorName());
					bookEntity.setCategory(bookDomain.getCategory());
					bookEntity.setPublisher(bookDomain.getPublisher());
					bookRepository.save(bookEntity);
				} else {
					ExceptionDomain exceptionDomain = new ExceptionDomain("Book does not exists", 1007);
					throw new LibraryServiceException(exceptionDomain);
				}
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid ISBNCode", 1008);
				throw new LibraryServiceException(exceptionDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid admin user", 1006);
			throw new LibraryServiceException(exceptionDomain);
		}

	}

	public BookDomain getBook(String isbnCode) {
		BookDomain bookDomain = null;
		if (isbnCode != null) {
			BookEntity bookEntity = bookRepository.findByISBNCode(isbnCode);
			if (bookEntity != null) {
				bookDomain = new BookDomain();
				bookDomain.setBookName(bookEntity.getBookName());
				bookDomain.setAuthorName(bookEntity.getAuthorName());
				bookDomain.setCategory(bookEntity.getCategory());
				bookDomain.setPublisher(bookEntity.getPublisher());
				bookDomain.setIsbnCode(bookEntity.getIsbnCode());
				bookDomain.setBookId(bookEntity.getBookId());
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("Book does not exists", 1007);
				throw new LibraryServiceException(exceptionDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid ISBNCode", 1008);
			throw new LibraryServiceException(exceptionDomain);
		}
		return bookDomain;

	}

	public List<BookDomain> getBooks() {
		List<BookDomain> bookDomains = null;
		List<BookEntity> bookEntities = bookRepository.findAll();
		if (!Collections.isEmpty(bookEntities)) {
			bookDomains = new ArrayList<BookDomain>();
			for (BookEntity bookEntity : bookEntities) {
				BookDomain bookDomain = new BookDomain();
				bookDomain.setBookName(bookEntity.getBookName());
				bookDomain.setAuthorName(bookEntity.getAuthorName());
				bookDomain.setCategory(bookEntity.getCategory());
				bookDomain.setPublisher(bookEntity.getPublisher());
				bookDomain.setIsbnCode(bookEntity.getIsbnCode());
				bookDomain.setBookId(bookEntity.getBookId());
				bookDomains.add(bookDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("No books found", 1008);
			throw new LibraryServiceException(exceptionDomain);
		}

		return bookDomains;

	}

	public void borrowBook(String isbnCode, long userId) {
		UserEntity userEntity = userService.fetchUser(userId);
		Timestamp today = new Timestamp(System.currentTimeMillis());
		boolean allowBorrow = false;
		if(userEntity.getBorrowOn() != null && today.getTime() > userEntity.getBorrowOn().getTime()) 
			allowBorrow = true;
		
		if (userEntity.isBorrowRight() && allowBorrow) {
			userEntity.setBorrowOn(null);
			userService.saveUserEntity(userEntity);
			BookEntity bookEntity = bookRepository.findByISBNCode(isbnCode);
			if (bookEntity != null) {
				BorrowEntity borrowEntity = new BorrowEntity();
				borrowEntity.setBookEntity(bookEntity);
				borrowEntity.setUserEntity(userEntity);
				borrowEntity.setBorrowedOn(new Timestamp(System.currentTimeMillis()));
				int FiveDaysInMinutes = 24 * 60 * 5;
				borrowEntity.setDueDate(
						new Timestamp(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(FiveDaysInMinutes)));
				borrowRepository.save(borrowEntity);
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid ISBNCode", 1008);
				throw new LibraryServiceException(exceptionDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("You cannot borrow a book upto "+userEntity.getBorrowOn().toString(), 1013);
			throw new LibraryServiceException(exceptionDomain);
		}

	}

	public void deleteBook(long bookId, long userId) {
		if (userService.adminUser(userId)) {
			BookEntity bookEntity = bookRepository.findOne(bookId);
			if (bookEntity != null) {
				BorrowEntity borrowEntity = borrowRepository.findByBookId(bookId);
				if (borrowEntity == null) {
					bookEntity.setDeleted(true);
					bookRepository.save(bookEntity);
				} else {
					ExceptionDomain exceptionDomain = new ExceptionDomain("Book is already borrowed by someone", 1009);
					throw new LibraryServiceException(exceptionDomain);
				}
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("Book does not exists", 1010);
				throw new LibraryServiceException(exceptionDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid admin user", 1006);
			throw new LibraryServiceException(exceptionDomain);
		}

	}
	
	public void returnBook(String isbnCode, long userId) {
		
			BookEntity bookEntity = bookRepository.findByISBNCode(isbnCode);
			if (bookEntity != null) {
				UserEntity userEntity = userService.fetchUser(userId);
				
				BorrowEntity borrowEntity = borrowRepository.findByBookIdAndUserIdAndNotRetured(bookEntity.getBookId(), userEntity.getUserId(), false);
				if(borrowEntity != null) {
					Timestamp today = new Timestamp(System.currentTimeMillis());
					if(today.getTime() > borrowEntity.getDueDate().getTime()) {
						long diffTime = today.getTime() - borrowEntity.getDueDate().getTime();
						userEntity.setBorrowOn(new Timestamp(today.getTime() + diffTime));
						userService.saveUserEntity(userEntity);
					}
					borrowEntity.setReturned(true);
					borrowEntity.setReturnedOn(new Timestamp(System.currentTimeMillis()));
				
					borrowRepository.save(borrowEntity);
				} else {
					ExceptionDomain exceptionDomain = new ExceptionDomain("Book borrow record does not exists", 1012);
					throw new LibraryServiceException(exceptionDomain);
				}
					
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid ISBNCode", 1008);
				throw new LibraryServiceException(exceptionDomain);
			}
		}


}
