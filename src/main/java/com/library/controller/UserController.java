package com.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.library.domains.BookDomain;
import com.library.domains.UserDomain;
import com.library.entities.UserEntity;
import com.library.repositories.UserRepository;
import com.library.servies.BookService;
import com.library.servies.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping(value="/fetch-user/{userId}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserEntity> fetchUser(@PathVariable("userId") String userId){
		return new ResponseEntity<UserEntity>(userService.fetchUser(Long.valueOf(userId)), HttpStatus.OK);
	}
	
	@RequestMapping(value="/authenticate/create-user",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> createUser(@RequestBody UserDomain userDomain){
		userService.createUser(userDomain);
		return new ResponseEntity<Object>("{\"User created successfully\":200}", HttpStatus.CREATED);
	}
							
	@RequestMapping(value="/authenticate/login-user",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDomain> loginUser(@RequestBody UserDomain userDomain){
		return new ResponseEntity<UserDomain>(userService.loginUser(userDomain), HttpStatus.OK);
	}
	
}
