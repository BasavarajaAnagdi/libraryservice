package com.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.library.entities.BookEntity;
import com.library.entities.BorrowEntity;

@Repository
public interface BorrowRepository extends JpaRepository<BorrowEntity,Long> , JpaSpecificationExecutor<BorrowEntity>{

	@Query(value = "select * from borrow where book_id = :bookId", nativeQuery=true)
	public BorrowEntity findByBookId(@Param("bookId") long bookId); 
	
	@Query(value = "select * from borrow where book_id = :bookId and user_id =:userId and returned =:returned", nativeQuery=true)
	public BorrowEntity findByBookIdAndUserIdAndNotRetured(@Param("bookId") long bookId, @Param("userId") long userId,@Param("returned") boolean returned);
	
}
