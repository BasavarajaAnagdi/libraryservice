package com.library.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="borrow")
public class BorrowEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="borrow_id")
	private long borrowId ;
	 
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private UserEntity userEntity;
	
	@OneToOne
	@JoinColumn(name = "book_id", referencedColumnName = "book_id")
	private BookEntity bookEntity;

	@Column(name = "borrowed_on")
	private Timestamp borrowedOn;

	@Column(name = "due_date")
	private Timestamp dueDate;

	@Column(name = "returned")
	private boolean returned;
	
	@Column(name = "returned_on")
	private Timestamp returnedOn;
	
	public long getBorrowId() {
		return borrowId;
	}

	public void setBorrowId(long borrowId) {
		this.borrowId = borrowId;
	}

	public UserEntity getUserEntity() {
		return userEntity;
	}

	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}

	public BookEntity getBookEntity() {
		return bookEntity;
	}

	public void setBookEntity(BookEntity bookEntity) {
		this.bookEntity = bookEntity;
	}

	public Timestamp getBorrowedOn() {
		return borrowedOn;
	}

	public void setBorrowedOn(Timestamp borrowedOn) {
		this.borrowedOn = borrowedOn;
	}

	public Timestamp getDueDate() {
		return dueDate;
	}

	public void setDueDate(Timestamp dueDate) {
		this.dueDate = dueDate;
	}

	public boolean isReturned() {
		return returned;
	}

	public void setReturned(boolean returned) {
		this.returned = returned;
	}

	public Timestamp getReturnedOn() {
		return returnedOn;
	}

	public void setReturnedOn(Timestamp returnedOn) {
		this.returnedOn = returnedOn;
	}
	
	
}
