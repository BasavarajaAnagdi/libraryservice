package com.library.servies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.library.domains.UserDomain;
import com.library.entities.UserEntity;
import com.library.exception.ExceptionDomain;
import com.library.exception.LibraryServiceException;
import com.library.repositories.UserRepository;
import com.library.utils.JwtTokenUtil;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	public UserEntity fetchUser(long userId) {
		UserEntity userEntity = userRepository.findOne(userId);
		if (userEntity != null) {
			return userEntity;
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid User", 1002);
			throw new LibraryServiceException(exceptionDomain);
		}

	}

	public void createUser(UserDomain userDomain) {

		UserEntity userEntity = userRepository.findByUserName(userDomain.getUserName());
		if (userEntity == null) {
			userEntity = new UserEntity();
			userEntity.setAdminUser(userDomain.isAdminUser());
			userEntity.setUserName(userDomain.getUserName());
			String encodedOldPassword = new BCryptPasswordEncoder(10).encode(userDomain.getPassword());
			userEntity.setPassword(encodedOldPassword);

			saveUserEntity(userEntity);

		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("User name is already taken, Create new one", 1001);
			throw new LibraryServiceException(exceptionDomain);
		}

	}

	public UserDomain loginUser(UserDomain userDomain) {
		
		UserEntity userEntity = userRepository.findByUserName(userDomain.getUserName());
		if (userEntity != null) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
			boolean passwordMatches = encoder.matches(userDomain.getPassword(), userEntity.getPassword());
			if (passwordMatches && userEntity.getUserName().equals(userDomain.getUserName())) {
				userDomain.setAdminUser(userEntity.isAdminUser());
				userDomain.setUserId(userEntity.getUserId());
				userDomain.setToken("library " + jwtTokenUtil.generateToken(userEntity.getUserName()));
			} else {
				ExceptionDomain exceptionDomain = new ExceptionDomain("User name or password is incorrect", 1003);
				throw new LibraryServiceException(exceptionDomain);
			}
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid User", 1002);
			throw new LibraryServiceException(exceptionDomain);
		}
		return userDomain;

	}

	public boolean adminUser(long userId) {
		UserEntity userEntity = userRepository.findOne(userId);
		if (userEntity != null) {
			return userEntity.isAdminUser();
		} else {
			ExceptionDomain exceptionDomain = new ExceptionDomain("Invalid User", 1002);
			throw new LibraryServiceException(exceptionDomain);
		}
	}
	
	public void saveUserEntity(UserEntity userEntity) {
		userRepository.save(userEntity);
	}
}
