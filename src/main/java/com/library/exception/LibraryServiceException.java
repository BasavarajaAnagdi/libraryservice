package com.library.exception;

import com.google.gson.GsonBuilder;


@SuppressWarnings("serial")
public class LibraryServiceException extends RuntimeException {
	private ExceptionDomain exceptionDomain;
	
	public LibraryServiceException(ExceptionDomain exceptionDomain) {
	    super(new GsonBuilder().setPrettyPrinting().create()
	    	      .toJson(exceptionDomain, ExceptionDomain.class));
		this.exceptionDomain = exceptionDomain;
	}

	public ExceptionDomain getExceptionDomain() {
		return exceptionDomain;
	}

	public void setExceptionDomain(ExceptionDomain exceptionDomain) {
		this.exceptionDomain = exceptionDomain;
	}
	
	
}
