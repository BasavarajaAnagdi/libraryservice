package com.library.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.library.domains.BookDomain;
import com.library.servies.BookService;

@RestController
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@RequestMapping(value="/book/{userId}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addBook(@PathVariable("userId") String userId, @RequestBody BookDomain bookDomain){
		bookService.addBook(bookDomain, Long.valueOf(userId));
		return new ResponseEntity<Object>("{\"Book added successfully\":200}", HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/book/{userId}",method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateBook(@PathVariable("userId") String userId, @RequestBody BookDomain bookDomain){
		bookService.updateBook(bookDomain, Long.valueOf(userId));
		return new ResponseEntity<Object>("{\"Book updated successfully\":200}", HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/book/{isbnCode}",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookDomain> getBook(@PathVariable("isbnCode") String isbnCode){
		return new ResponseEntity<BookDomain>(bookService.getBook(isbnCode), HttpStatus.OK);
	}

	@RequestMapping(value="/books",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<BookDomain>> getBooks(){
		return new ResponseEntity<List<BookDomain>>(bookService.getBooks(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/book/borrow/{isbnCode}/{userId}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> borrowBook(@PathVariable("isbnCode") String isbnCode, @PathVariable("userId") String userId){
		bookService.borrowBook(isbnCode, Long.valueOf(userId));
		return new ResponseEntity<Object>("{\"Book borrowed successfully\":200}", HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/book/{bookId}/{userId}",method = RequestMethod.DELETE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteBook(@PathVariable("bookId") String bookId, @PathVariable("userId") String userId){
		bookService.deleteBook(Long.valueOf(bookId), Long.valueOf(userId));
		return new ResponseEntity<Object>("{\"Book deleted successfully\":200}", HttpStatus.OK);
	}
	
	@RequestMapping(value="/book/return/{isbnCode}/{userId}",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> returnBook(@PathVariable("isbnCode") String isbnCode, @PathVariable("userId") String userId){
		bookService.returnBook(isbnCode, Long.valueOf(userId));
		return new ResponseEntity<Object>("{\"Book returned successfully\":200}", HttpStatus.CREATED);
	}
	
}
