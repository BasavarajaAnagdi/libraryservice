package com.library.domains;

public class UserDomain {

	private long userId ;
	private String userName; 
	private String password;
	private boolean adminUser;
	private String token;
	private boolean borrowRight;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isAdminUser() {
		return adminUser;
	}

	public void setAdminUser(boolean adminUser) {
		this.adminUser = adminUser;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	public boolean isBorrowRight() {
		return borrowRight;
	}

	public void setBorrowRight(boolean borrowRight) {
		this.borrowRight = borrowRight;
	}

}
