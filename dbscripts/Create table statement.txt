spring.datasource.url = jdbc:mysql://localhost:3306/library?useSSL=false
spring.datasource.username = root
spring.datasource.password = 1234


use library;

CREATE TABLE `user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `admin_user` tinyint(1) DEFAULT '0',
  `borrow_right` tinyint DEFAULT '1',
  `borrow_on` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
);


CREATE TABLE `books` (
  `book_id` bigint NOT NULL AUTO_INCREMENT,
  `book_name` varchar(50) NOT NULL,
  `author_name` varchar(50) NOT NULL,
  `isbn_code` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `publisher` varchar(50) NOT NULL,
  `deleted` tinyint DEFAULT '0',
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `isbn_code` (`isbn_code`)
);

CREATE TABLE `borrow` (
  `borrow_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL DEFAULT '0',
  `book_id` bigint NOT NULL DEFAULT '0',
  `borrowed_on` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `returned` tinyint DEFAULT '0',
  `returned_on` datetime DEFAULT NULL,
  PRIMARY KEY (`borrow_id`),
  KEY `user_id` (`user_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `borrow_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `borrow_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`)
);


