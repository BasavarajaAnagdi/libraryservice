package com.library.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.library.entities.BookEntity;

@Repository
public interface BookRepository extends JpaRepository<BookEntity,Long> , JpaSpecificationExecutor<BookEntity>{

	@Query(value = "select * from books where isbn_code = :isbnCode", nativeQuery=true)
	public BookEntity findByISBNCode(@Param("isbnCode") String isbnCode);
}
