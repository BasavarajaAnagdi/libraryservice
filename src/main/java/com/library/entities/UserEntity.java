package com.library.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class UserEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="user_id")
	private long userId ;
	 

	@Column(name = "user_name")
	private String userName; 

	@Column(name = "password")
	private String password;
	
	@Column(name = "admin_user")
	private boolean adminUser;

	@Column(name = "borrow_right")
	private boolean borrowRight;
	
	@Column(name = "borrow_on")
	private Timestamp borrowOn;
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isAdminUser() {
		return adminUser;
	}

	public void setAdminUser(boolean adminUser) {
		this.adminUser = adminUser;
	}

	public boolean isBorrowRight() {
		return borrowRight;
	}

	public void setBorrowRight(boolean borrowRight) {
		this.borrowRight = borrowRight;
	}

	public Timestamp getBorrowOn() {
		return borrowOn;
	}

	public void setBorrowOn(Timestamp borrowOn) {
		this.borrowOn = borrowOn;
	}
	
	

}
