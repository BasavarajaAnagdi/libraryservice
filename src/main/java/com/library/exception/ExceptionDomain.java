package com.library.exception;

import java.util.HashMap;

import org.springframework.http.HttpStatus;

public class ExceptionDomain {
	private String errorMessage;
	private long errorCode;
	private HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	
	
	public ExceptionDomain(String errorMessage, long errorCode) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}


	public String getErrorMessage() {
		return errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public long getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}


	public HttpStatus getStatus() {
		return status;
	}


	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	
}


